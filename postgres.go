package blitz

import (
	"database/sql"
	"fmt"
	"log"
	"os"

	"github.com/caarlos0/env"
	// Bad design?
	_ "github.com/lib/pq"
)

type config struct {
	Name     string `env:"POSTGRES_DB"`
	Host     string `env:"POSTGRES_HOST"`
	User     string `env:"POSTGRES_USER"`
	Password string `env:"POSTGRES_PASSWORD"`
}

// DB ...
var DB *sql.DB

// Postgres returns a *sql.DB value
func Postgres() (err error) {
	if os.Getenv("GO_ENV") != "production" {
		log.Println("[WARNIG] Using mock database")
		return
	}

	var dbConfig config

	env.Parse(&dbConfig)

	dbInfo := fmt.Sprintf("host=%s dbname=%s user=%s password=%s sslmode=disable",
		dbConfig.Host, dbConfig.Name, dbConfig.User, dbConfig.Password)

	log.Println("Connecting to postgres at", dbConfig.Host)

	DB, err = sql.Open("postgres", dbInfo)
	if err != nil {
		log.Panic(err)
	}

	err = DB.Ping()
	if err != nil {
		log.Println(err)
	}

	return
}
